﻿DROP DATABASE IF EXISTS ejemplo10;
CREATE DATABASE IF NOT EXISTS ejemplo10;
USE ejemplo10;

CREATE TABLE noticias(
  codigo int AUTO_INCREMENT,
  texto varchar(1000),
  fecha date,
  PRIMARY KEY(codigo)
  );

CREATE TABLE comentarios(
  codigo int AUTO_INCREMENT,
  texto varchar(1000),
  fecha date,
  cod_noticia int NOT NULL,
  PRIMARY KEY(codigo),
  CONSTRAINT fk_com_noticias FOREIGN KEY(cod_noticia) REFERENCES noticias(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE fotos(
  codigo int AUTO_INCREMENT,
  nombre varchar(100),
  PRIMARY KEY(codigo)
  );

CREATE TABLE not_fotos(
  cod_fotos int,
  cod_noticias int,
  visitas int DEFAULT 0,
  PRIMARY KEY(cod_fotos,cod_noticias),
  CONSTRAINT fk_notfot_fotos FOREIGN KEY(cod_fotos) REFERENCES fotos(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fk_notfot_noticias FOREIGN KEY(cod_noticias) REFERENCES noticias(codigo)
  ON DELETE CASCADE ON UPDATE CASCADE 
  );

