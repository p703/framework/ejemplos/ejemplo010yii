<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    "model"=>$model
]);
?>
<div>
<?php
//colocar boton para eliminar la noticia
echo Html::a(//'<i class="fas fa-exclamation-triangle"></i>',
        "¿Estas seguro de eliminar la noticia?",
        ["site/eliminarnoticia","codigo"=>$model->codigo],
        ["class"=>"btn btn-danger col-lg-4"]);

?>
</div>