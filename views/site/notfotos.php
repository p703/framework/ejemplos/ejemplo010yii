<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\notFotos */
/* @var $form ActiveForm */
?>
<div class="site-notfotos">

    <?php $form = ActiveForm::begin(); ?>
    
    <?php 
    
    //colocar yodas las noticias en la variable para ,ostrarlas en el deplegable
    $noticias= app\models\Noticias::find()->all();

    $listData= yii\helpers\ArrayHelper::map($noticias,'codigo',function($model){
        return $model->codigo . " - " . $model->titulo;//en el desplegable colocamos el codigo y titulo de la noticia
    });

    echo $form->field($model, 'cod_noticias')->dropDownList(
            $listData,
            ['prompt'=>'Selecciona una noticia']
    );
    ?>
    <?php 
    
    //almacenar en la variable todas las fotos de la tabla fotos
    $fotos= app\models\Fotos::find()->all();

    /*OPCION 1*/
    /*MOSTRAR SOLAMENTE EL CODIGO Y NOMBRE DE LA IMAGEN*/
    
    //creando variable con el codigo y el nombre de cada una de las fotos
    /*$listData= yii\helpers\ArrayHelper::map($fotos,'codigo',function($model){
        return $model->codigo . "-" . $model->nombre;
    });*/

    /*echo $form->field($model, 'cod_fotos')->dropDownList(
            $listData,
            ['prompt'=>'Selecciona una imagen']
    );*/
    
    /*FIN OPCION 1*/
    
    /*OPCION 2*/
    /*MOSTRAR EN LISTADO TODAS LAS IMAGENES*/
    
    //creo una variable con las img de todas las fotos de la base de datos
    $listData= yii\helpers\ArrayHelper::map($fotos,'codigo',function($model){
        return Html::img("@web/imgs/" . $model->nombre,["class"=>'col-lg-10 img-fluid']);
    });
    
    //listar tosas las imagenes en un listado de casillas de verificacion
    echo $form->field($model, 'cod_fotos',['options'=>['class'=>'col-lg-3 p-0']])->checkboxList(
            $listData,
            ['prompt'=>'Selecciona una imagen','encode'=>false]
        );
    
    /*FIN OPCION 2*/

    ?>

    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
